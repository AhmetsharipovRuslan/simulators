﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public GameObject startUI;
    public GameObject beltsUI;
    public GameObject wireUI;
    public GameObject connectionUI;
    public float time = 3f;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(turnOff(startUI));
    }

    IEnumerator turnOff(GameObject go)
    {
        yield return new WaitForSeconds(time);
        go.SetActive(false);
    }
    public void TurnOnText()
    {
        StartCoroutine(turnOn());
    }
    IEnumerator turnOn()
    {
        yield return new WaitForSeconds(time);
        beltsUI.SetActive(true);
        wireUI.SetActive(true);
        connectionUI.SetActive(true);
    }
}

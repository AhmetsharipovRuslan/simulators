﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMouseHiglight : ScenarioStep
{
    public GameObject go;
    public GameObject go1;
    bool initiated = false;
    public override bool DoAction(Transform interactionArea = null)
    {
        if (!initiated)
        {
            var temp = go.GetComponent<OnObjectClick>();
            temp.onMouseEnter.RemoveAllListeners();
            temp.onMouseExit.RemoveAllListeners();
            temp.onMouseEnter.AddListener(()=>{
                if (go1!=null)
                    go1.SetActive(true);
            });
            temp.onMouseExit.AddListener(() =>
            {
                if (go1!=null)
                    go1.SetActive(false);
            });
            initiated = true;
        }
        isDone = true;
        return true;
    }
}

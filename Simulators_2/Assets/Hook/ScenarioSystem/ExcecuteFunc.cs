﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExcecuteFunc : ScenarioStep
{
    static Dictionary<string, GameObject> tempInstance = new Dictionary<string, GameObject>();
    static Dictionary<string, TextAsset> cachedAssets = new Dictionary<string, TextAsset>();
    ScenarioController scenarioController;
    public string path;
    public override bool DoAction(Transform interactionArea = null)
    {
        if (scenarioController == null)
        {
            Init();
            return false;
        }
        isDone = scenarioController.IsScenarioEnd();
        if (isDone)
            GameObject.Destroy(scenarioController);
        return isDone;
    }

    public void Init()
    {
        if (!tempInstance.ContainsKey(path) || tempInstance[path] == null || scenarioController.gameObject == null)
        {
            GameObject t = new GameObject("FunctionInstance(TEMP)");
            scenarioController = t.AddComponent<ScenarioController>();
            if (!tempInstance.ContainsKey(path))
                tempInstance.Add(path, t);
            else
                tempInstance[path] = t;
            var textAssets = Resources.FindObjectsOfTypeAll<TextAsset>();
            for (int i = 0; i < textAssets.Length; i++)
            {
                if (textAssets[i].name == path)
                {
                    scenarioController.SetTextAsset(path, textAssets[i]);
                    if (cachedAssets.ContainsKey(path))
                        cachedAssets[path] = textAssets[i];
                    else 
                        cachedAssets.Add(path, textAssets[i]);
                    scenarioController.Load();
                    return;
                }
            }
        }
        else
        {
            scenarioController = tempInstance[path].AddComponent<ScenarioController>();
            scenarioController.SetTextAsset(path, cachedAssets[path]);
            scenarioController.Load();
            scenarioController.Start();
            return;
        }
        Debug.Log("Function not found");
    }
}

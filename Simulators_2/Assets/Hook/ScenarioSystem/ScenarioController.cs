﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class ScenarioController : MonoBehaviour {
    public string scenarioPath = "/test.txt";
    public string[] functionPaths;

    internal void RemoveActiveStep(int id)
    {
        activeSteps.Remove(id);
    }

    public string ScenarioPath {
        get {
            return scenarioPath;
        }

        set {
#if UNITY_EDITOR
            Debug.Log("set");
            scenario = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets"+value);
#endif
            scenarioPath = value;
        }
    }

    public string[] FunctionPaths {
        get {
            return functionPaths;
        }

        set {
            functionPaths = value;
#if UNITY_EDITOR
            functions = new TextAsset[functionPaths.Length];
            for (int i = 0; i < functionPaths.Length; i++)
            {
                functions[i] = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets" + functionPaths[i]);
            }
#endif
        }
    }
    public TextAsset scenario;
    public TextAsset[] functions;
    public List<ScenarioStep> steps;
#if UNITY_EDITOR
    public List<Rect> rects;
    public Vector2 offset = Vector2.zero;
#endif
    [Serializable]
    public class ScenarioContainer
    {
        public ScenarioStep[] steps;
        public ScenarioContainer() { }
    }
    void Awake () {
        ScenarioSaver.BuildGameObjectMap();
        Load();
	}
    List<int> activeSteps;	
	// Update is called once per frame
	void Update () {
        if (steps.Count == 0)
            return;
        for (int i = activeSteps.Count-1; i >=0; i--)
        {
            int id = activeSteps[i];
            ScenarioStep step = steps[id];

            if (step.isPreviousDone(this) && step.DoAction())
            {
                foreach (var j in steps[id].next)
                    if (!activeSteps.Contains(j))
                        activeSteps.Add(j);
                activeSteps.Remove(id);
                if (step.GetType() == typeof(RemoveActiveStep))
                    break;
            }
        }
	}

    public void Start()
    {
        activeSteps = new List<int>();
        activeSteps.Add(0);
    }

    public bool IsScenarioEnd()
    {
        return activeSteps.Count == 0;
    }

    public bool IsScenarioReferenceOK()
    {
        return scenario != null;
    }

    public void Save()
    {
            ScenarioContainer sc = new ScenarioContainer();
            sc.steps = steps.ToArray();
#if UNITY_EDITOR
            File.WriteAllText(Application.dataPath + scenarioPath, ScenarioSaver.SerializeObject(sc));
        scenario = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets" + scenarioPath);
        AssetDatabase.Refresh();
#endif
    }

    public void Load()
    {
        try
       {
        if (scenario == null)
            Save();
            string json = scenario.text;
            ScenarioContainer sc = new ScenarioContainer();
            ScenarioSaver.BuildGameObjectMap();
            sc = (ScenarioContainer)ScenarioSaver.Deserialize(json);
            if (sc.steps != null)
                steps = new List<ScenarioStep>(sc.steps);
            else
                steps = new List<ScenarioStep>();
            for (int i = 0; i < steps.Count; i++)
            {
                var temp = steps[i].next;
                for (int j = 0; j < temp.Length; j++)
                    steps[temp[j]].AddPrev(i);
            }
            if (rects == null)
                rects = new List<Rect>();
        }
        catch (Exception e)
        {
            steps = new List<ScenarioStep>();
            Debug.Log(e.Message);
        }
    }

#if UNITY_EDITOR
    public void UpdateFunctionLinks()
    {
        try
        {
            if (functionPaths == null)
                return;
            functions = new TextAsset[functionPaths.Length];
            for (int i = 0; i < functionPaths.Length; i++)
            {
                functions[i] = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets" + functionPaths[i]);
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Function not found");
        }
    }
#endif

    public void SetTextAsset(string path, TextAsset textAsset)
    {
        this.scenario = textAsset;
        this.scenarioPath = path;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wait : ScenarioStep {
    public float time=0f;
    public override bool DoAction(Transform interactionArea = null)
    {
        if (isDone == false)
        {
            time -= Time.deltaTime;
            isDone = time < 0;
        }
        return isDone;
    }
}

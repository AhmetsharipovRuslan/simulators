﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class ScenarioSaver {

    static System.Type stringType = typeof(string);

    public static string SerializeObject(object obj)
    {
        if (obj == null)
            return "";
        string result = "{ ";
            result += "\"type\" : " + '\"' + obj.GetType().FullName + '\"';
        if (obj.GetType() == typeof(GameObject))
        {
            result += ", \"InstanceId\" : \"" + GetGOInstanceID((GameObject)obj)+ "\" }";
            return result;
        }
        FieldInfo[] fieldsInfo = obj.GetType().GetFields();
        if (fieldsInfo.Length > 0)
            result += ", ";
        for (int i = 0; i < fieldsInfo.Length; i++)
        {
            var v = fieldsInfo[i];
            System.Type type = v.FieldType;
            System.Object value = v.GetValue(obj);
            if (type.IsValueType || type == stringType)
                result += '\"' + v.Name + "\" : \"" + value + '\"';
            else
            if (type.IsArray)
                result += '\"' + v.Name + "\" : [" + SerializeArray((IEnumerable)(value)) + " ]";
            else {
                if (value!=null)
                   result += '\"' + v.Name + "\" : " + SerializeObject(value);
            }
            if (i < fieldsInfo.Length - 1 && value!=null)
                result += " , ";
        }
        result += " }";
        return result;
    }

    public static string GetGOInstanceID(GameObject go)
    {
        ScenarioSystemGO scenarioSystemGO = go.GetComponent<ScenarioSystemGO>();
        if (scenarioSystemGO == null)
        {
            scenarioSystemGO = go.AddComponent<ScenarioSystemGO>();
            scenarioSystemGO.id = System.Guid.NewGuid().ToString();
        }
        return scenarioSystemGO.id;
    }

    public static Dictionary<string, GameObject> m_instanceMap = new Dictionary<string, GameObject>();

    public static void BuildGameObjectMap()
    {
        m_instanceMap.Clear();
        List<GameObject> gos = new List<GameObject>();
        foreach (ScenarioSystemGO go in Resources.FindObjectsOfTypeAll(typeof(ScenarioSystemGO)))
        {
            if (gos.Contains(go.gameObject))
            {
                continue;
            }
            gos.Add(go.gameObject);
            m_instanceMap[go.id] = go.gameObject;
        }
    }

    static string SerializeArray(IEnumerable arr)
    {
        if (arr == null)
            return "";
        string result = "";
        int i = 0;
       foreach(var temp in arr)
        {
            System.Type type = temp.GetType();
            if (i > 0)
                result += " , ";
            if (type.IsValueType || type == stringType)
            {
                result += '\"' + temp.ToString() + '\"';
            }
            else
                result += SerializeObject(temp);
            i++;
        }
        return result;
    }

    public enum ParsingStates {ReadType, ReadObject, ReadField, ReadValue, ReadMassive}
    public static object Deserialize(string s)
    {
        int i = 1;
        string fieldName = GetFieldName(s, ref i);
        string type = "";
        if (fieldName == "type")
        {
            type = GetStringValue(s, ref i);
        }
        return DeserializeObject(type, s, ref i);
    }

    public static object DeserializeObject(string type, string s, ref int pos)
    {
        if (type == "UnityEngine.GameObject")
        {
            string fieldName = GetFieldName(s, ref pos);
            string val = GetFieldName(s, ref pos);
            while (pos < s.Length && s[pos] != '}')
                pos++;
            if (!m_instanceMap.ContainsKey(val))
                return null;
            return m_instanceMap[val];
        }
        object v = Activator.CreateInstance(System.Type.GetType(type));
        ParsingStates currentState = ParsingStates.ReadField;
        string lastFieldName ="";
        FieldInfo[]  fieldsInfo = v.GetType().GetFields();
        bool insideString = false;
        while (pos < s.Length && s[pos] != '}')
        {
            if (s[pos] == '\"')
                insideString=!insideString;

            if (!HaveToIgnore(s[pos]) || insideString)
            {
                switch (currentState)
                {
                    case ParsingStates.ReadField:
                        if (s[pos] == '\"')
                        {
                            lastFieldName = GetFieldName(s, ref pos);
                            currentState = ParsingStates.ReadValue;
                            insideString = false;
                        }
                        break;
                    case ParsingStates.ReadValue:
                        if (s[pos] == '\"')
                        {
                            int p = GetFieldId(fieldsInfo, v, lastFieldName);
                            System.Type fieldType = fieldsInfo[p].FieldType;
                            object primitive = StringToPrimitive(fieldType, GetStringValue(s, ref pos));
                            fieldsInfo[p].SetValue(v, primitive);
                            currentState = ParsingStates.ReadField;
                        }
                        else
                        if (s[pos] == '{')
                        {
                            pos++;
                            string firstField = GetFieldName(s, ref pos);
                            int p = GetFieldId(fieldsInfo, v, lastFieldName);
                            if (firstField == "type")
                            {
                                object obj = DeserializeObject(GetStringValue(s, ref pos), s, ref pos);
                                string t = fieldsInfo[p].Name;
                                fieldsInfo[p].SetValue(v, obj);
                            }
                            currentState = ParsingStates.ReadField;
                        }
                        else
                        if (s[pos] == '[')
                        {
                            pos++;
                            currentState = ParsingStates.ReadMassive;
                        }
                        break;
                    case ParsingStates.ReadMassive:
                        fieldsInfo = v.GetType().GetFields();
                        System.Type arrayType = typeof(Int32);
                        //lastFieldName = GetFieldName(s, ref pos);
                        FieldInfo fieldInfo = null;
                        foreach (var f in fieldsInfo) {
                            if (f.Name == lastFieldName) {
                                arrayType = f.FieldType.GetElementType();
                                fieldInfo = f;
                                break;
                            }
                        }

                        var listType = typeof(List<>);
                        var constructedListType = listType.MakeGenericType(arrayType);
                        var array = (IList)Activator.CreateInstance(constructedListType);
                        if (arrayType.IsPrimitive || arrayType == stringType)
                        {
                            while (pos < s.Length && s[pos] != ']')
                            {
                                //  string fieldName = GetFieldName(s, ref pos);
                                string val = GetStringValue(s, ref pos);
                                if (val != "")
                                    array.Add(StringToPrimitive(arrayType, val));
                            }
                        }
                        else {
                            while (pos < s.Length && s[pos] != ']')
                            {
                                string fieldName = GetFieldName(s, ref pos);
                                string objType = "";
                                if (fieldName == "type")
                                {
                                    objType = GetStringValue(s, ref pos);
                                    object element = DeserializeObject(objType, s, ref pos);
                                    array.Add(element);
                                }
                            }
                        }

                        currentState = ParsingStates.ReadField;

                        if (fieldInfo != null)
                        {
                            Array arr = Array.CreateInstance(arrayType, array.Count);
                            array.CopyTo(arr, 0);
                            fieldInfo.SetValue(v, arr);
                        }
                       // if (s[pos] == '\"')
                        //   array.Add()
                        break;
                }
            }
            pos++;
        }

        return v;
    }

    static string ignoreChars = "\b\f\n\t\v\n :,";

    static bool HaveToIgnore(char c)
    {
        for (int i = 0; i < ignoreChars.Length; i++)
            if (ignoreChars[i] == c)
                return true;
        return false;
    }

    public static Vector3 getVector3(string rString)
    {
        string[] temp = rString.Substring(1, rString.Length - 2).Split(',');
        float x = float.Parse(temp[0]);
        float y = float.Parse(temp[1]);
        float z = float.Parse(temp[2]);
        Vector3 rValue = new Vector3(x, y, z);
        return rValue;
    }

    static int GetFieldId(FieldInfo[] fieldsInfo, object v, string fieldName)
    {
        fieldsInfo = v.GetType().GetFields();
        for (int i = 0; i < fieldsInfo.Length; i++)
            if (fieldsInfo[i].Name == fieldName)
                return i;
        return 0;
    }

    static object StringToPrimitive(System.Type type, string s)
    {
        try {
            if (type == typeof(Int32))
                return Int32.Parse(s);
            if (type == typeof(Single))
                return Single.Parse(s);
            if (type == typeof(Boolean))
                    return s != "False";
            if (type == typeof(Vector3))
                return getVector3(s);
        } catch (Exception e)
        {
            Debug.Log(s);
        }
        return s;
    }

    static string GetFieldName(string s, ref int pos)
    {
        if (s[pos] == '\'' && s[pos + 1] == '\'')
            return "";
        while (HaveToIgnore(s[pos]) || s[pos] == '\"' )
           pos++;

        if (s[pos] == ']')
            return "";
        string fieldName = "";
        while (s[pos] != '\"' && s[pos] !='}')
        {
            fieldName += s[pos];
            pos++;
        }
        pos++;
        return fieldName;
    }

    static string GetStringValue(string s, ref int pos)
    {
        return GetFieldName(s, ref pos);
    }

    static string GetNumericString(string s, ref int pos)
    {
        string numericString = "";
        while (!HaveToIgnore(s[pos]))
        {
            numericString += s[pos];
            pos++;
        }
        return numericString;
    }

}

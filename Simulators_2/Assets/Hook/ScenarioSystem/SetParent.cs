﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SetParent : ScenarioStep
{
    public GameObject obj;
    public GameObject parent;
    public override bool DoAction(Transform interactionArea = null)
    {
        obj.transform.SetParent(parent.transform);
        return true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectClick : ScenarioStep {
    public GameObject go;
    bool actionAdded = false;
    public override bool DoAction(Transform interactionArea = null)
    {
        if (!actionAdded)
        {
            actionAdded = true;
            go.GetComponent<OnObjectClick>().onObjectClick.AddListener(() =>
            {
                isDone = true;
            });
        }
        return isDone;
    }
}

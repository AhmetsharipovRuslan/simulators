﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnObjectClick : MonoBehaviour
{
    ObjectsOutline outline;
    public float width = 5f;
    public UnityEvent onObjectClick;
    public UnityEvent onMouseEnter;
    public UnityEvent onMouseExit;
    private animatorController control;
    public void Awake()
    {
        outline = GetComponent<ObjectsOutline>();
        control = GetComponent<animatorController>();
    }

    public void OnMouseEnter()
    {
        outline.OutlineWidth = width;
        if (onMouseEnter != null)
            onMouseEnter.Invoke();
    }

    public void OnMouseExit()
    {
        outline.OutlineWidth = 0f;
        if (onMouseExit != null)
            onMouseExit.Invoke();
    }

    public void OnMouseDown()
    {
        if (onObjectClick != null)
            onObjectClick.Invoke();
        if (control != null)
        {
            control.setAnimatorValue();
        }
        Debug.Log("mouseDown");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActivation : ScenarioStep {
    public GameObject obj;
    public bool activation;
    public override bool DoAction(Transform interactionArea = null)
    {
        obj.SetActive(activation);
        isDone = true;
        return true;
    }
}

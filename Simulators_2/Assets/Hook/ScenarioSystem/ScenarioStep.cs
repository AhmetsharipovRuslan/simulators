﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;
#endif
using UnityEngine;

[Serializable]
public class ScenarioStep {
    public int id;
    public int[] next;
    public bool isDone;
    public ScenarioStep() {

    }

    private List<int> prev = new List<int>();
    public List<int> Prev {
        get {
            return prev;
        }
        set {
            prev = value;
        }
    }

    public void AddPrev(int num)
    {
        if (!prev.Contains(num))
            prev.Add(num);
    }

    public virtual bool DoAction(Transform interactionArea = null) {
        return true;
    }

    public bool isPreviousDone(ScenarioController scenarioController)
    {
        for (int i = 0; i < prev.Count; i++)
            if (!scenarioController.steps[prev[i]].isDone)
                return false;
        return true;
    }

    public void AddNext(int id, ScenarioController scenarioController)
    {
        //next = id;
        if (next == null)
            next = new int[0];
        if (Array.IndexOf(next, id) == -1)
        {
            int len = next.Length;
            Array.Resize(ref next, len + 1);
            next[len] = id;
            scenarioController.steps[id].AddPrev(id);
        }
    }



    public void RemoveNextId(int clickId)
    {
        bool found = false;
        if (next != null)
        {
            int l = next.Length;
            for (int i = 0; i < l; i++)
            {
                if (!found && next[i] == clickId)
                    found = true;
                if (found && i + 1 < l)
                    next[i] = next[i + 1];
            }
            if (l>0)
                Array.Resize(ref next, l - 1);
        }
    }

    public void RemovePrevsId(int id)
    {
        prev.Remove(id);
    }

#if UNITY_EDITOR
    public virtual void DrawEditorGUI(Rect offsetedRect, ScenarioController scenarioController)
    {
        GUILayout.BeginArea(offsetedRect);
        Rect r = new Rect(0f, 0f, offsetedRect.width, offsetedRect.height);
        GUI.Box(r, "");
        GUILayoutOption width = GUILayout.Width(offsetedRect.width - 5);
        EditorGUI.indentLevel = 1;
        GUILayout.BeginVertical();
        FieldInfo[] fieldsInfo = this.GetType().GetFields();
        EditorGUILayout.LabelField(this.GetType().Name + " " + this.id);
        float height = 14f;
        foreach (var field in fieldsInfo)
        {
            if (field.Name != "id" && field.Name!="isDone")
            {
                height += EditorGUIUtility.singleLineHeight;
                EditorGUIUtility.labelWidth = 50f;
                GUILayoutOption valueWidth = GUILayout.Width(offsetedRect.width - 10f);
                if (field.FieldType == typeof(Int32)) 
                    field.SetValue(this, EditorGUILayout.IntField(field.Name, (Int32)(field.GetValue(this)), valueWidth));
                else 
                if (field.FieldType == typeof(String))
                    field.SetValue(this, EditorGUILayout.TextField(field.Name, (String)(field.GetValue(this)), valueWidth));
                else
                if (field.FieldType == typeof(Single))
                    field.SetValue(this, EditorGUILayout.FloatField(field.Name, (Single)(field.GetValue(this)), valueWidth));
                else
                if (field.FieldType == typeof(Boolean))
                    field.SetValue(this, EditorGUILayout.Toggle(field.Name, (Boolean)(field.GetValue(this)), valueWidth));
                else
                if (field.FieldType == typeof(GameObject))
                    field.SetValue(this, EditorGUILayout.ObjectField(field.Name, (UnityEngine.Object)field.GetValue(this), typeof(GameObject), true, valueWidth));
                else
                if (field.FieldType == typeof(Vector3))
                {
                    Vector3 temp = (Vector3)field.GetValue(this);
                    temp = EditorGUILayout.Vector3Field(field.Name, temp, valueWidth);
                    field.SetValue(this, temp);
                    height += EditorGUIUtility.singleLineHeight;
                }
            }
        }
        Rect rr = scenarioController.rects[id];
        rr.height = height;
        scenarioController.rects[id] = rr;
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
#endif
}

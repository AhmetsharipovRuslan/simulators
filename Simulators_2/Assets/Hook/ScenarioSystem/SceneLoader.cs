﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
    public int sceneNum;
    public void LoadScene(string name) {
        SceneManager.LoadScene(sceneNum);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            LoadScene("hook_1");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogMessage : ScenarioStep {
    public string message;
    public override bool DoAction(Transform interactionArea = null)
    {
        Debug.Log(message);
        return true;
    }
}

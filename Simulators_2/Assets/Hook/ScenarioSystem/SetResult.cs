﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetResult : ScenarioStep
{
    public GameObject go;
    public int result;
    public override bool DoAction(Transform interactionArea = null)
    {
        go.GetComponent<ResultShower>().SetResult(result);
        isDone = true;
        return true;
    }
}

﻿using UnityEngine;

public class SetObjectTransform : ScenarioStep
{
    public GameObject obj;
    public string transformName;
    public Vector3 value;
    public override bool DoAction(Transform interactionArea = null)
    {
        switch (transformName.ToLower())
        {
            case "localposition":
                obj.transform.localPosition = value;
                break;
            case "position":
                obj.transform.position = value;
                break;
            case "localscale":
                obj.transform.localScale = value;
                break;
            case "localrotation":
                obj.transform.localRotation = Quaternion.Euler(value);
                break;
            case "rotation":
                obj.transform.rotation = Quaternion.Euler(value);
                break;
            case "up":
                obj.transform.up = value;
                break;
            case "forward":
                obj.transform.forward = value;
                break;
            case "right":
                obj.transform.right = value;
                break;
        }
        return true;
    }
}

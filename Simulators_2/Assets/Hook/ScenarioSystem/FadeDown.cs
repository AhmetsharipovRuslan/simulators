﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeDown : MonoBehaviour {
    Image[] images;
    Text[] texts;
    public float duration = 1f; 
	void Start () {
        images = transform.GetComponentsInChildren<Image>();
        texts = transform.GetComponentsInChildren<Text>();
        foreach (var img in images)
            img.CrossFadeAlpha(0, duration, false);
        foreach (var text in texts)
            text.CrossFadeAlpha(0, duration, false);
    }
	
	
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class InstantiatePrefab : ScenarioStep
{
    public GameObject prefab;
    public GameObject parent;
    public Vector3 position;
    public Vector3 rotation;
    public override bool DoAction(Transform interactionArea = null)
    {
        GameObject.Instantiate(prefab, position, Quaternion.Euler(rotation), parent.transform);
        return true;
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckTrigger : ScenarioStep
{
    public GameObject first;
    public GameObject second;
    public override bool DoAction(Transform interactionArea = null)
    {
        isDone = first.GetComponent<LastTriggerEnter>().lastObj == second;
        return isDone;
    }
}


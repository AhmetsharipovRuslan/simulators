﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ScenarioController))]
public class ScenarioControllerInspector : Editor {
    Color green = new Color(0f, 0.8f, 0f, 1f);
    Color red = new Color(0.8f, 0f, 0f, 1f);
    public override void OnInspectorGUI()
    {
        ScenarioController sc = target as ScenarioController;
        string s = EditorGUILayout.TextField("Path:", sc.ScenarioPath);
        SerializedProperty property = serializedObject.FindProperty("functionPaths");
        EditorGUILayout.PropertyField(property, new GUIContent("FunctionPaths"), true);
        serializedObject.ApplyModifiedProperties();
        sc.UpdateFunctionLinks();
        if (s != sc.ScenarioPath)
            sc.ScenarioPath = s;
        GUIStyle textStyle = new GUIStyle();
        if (sc.IsScenarioReferenceOK()) {
            textStyle.normal.textColor = green;
            EditorGUILayout.LabelField("File succesfully linked.", textStyle);
        } else
        {
            textStyle.normal.textColor = red;
            EditorGUILayout.LabelField("File doesn`t exist. It will create automatically in target path.", textStyle);
        }
        GUI.contentColor = Color.black;
        if (GUILayout.Button("Edit Scenario"))
        {
            ScenarioNodeEditor.ShowWindow(sc);
        }
    }	
}

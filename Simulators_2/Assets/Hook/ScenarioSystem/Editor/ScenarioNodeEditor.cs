﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System;
using UnityEditor.SceneManagement;
using System.Linq;
using System.Reflection;

public class ScenarioNodeEditor : EditorWindow {
    public ScenarioController scenarioController;
    public static ScenarioNodeEditor window;
    public static IEnumerable<System.Type> stepTypes;
    List<Rect> addConnectionRects;
    public static void ShowWindow(ScenarioController scenarioController)
    {
        window = EditorWindow.GetWindow(typeof(ScenarioNodeEditor)) as ScenarioNodeEditor;
        scenarioController.Load();
        window.scenarioController = scenarioController;
        stepTypes = AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(assembly => assembly.GetTypes())
                       .Where(type => type.IsSubclassOf(typeof(ScenarioStep)));
    }

    Vector2 lastMousePos = Vector2.zero;
    Vector2 lastMouseClick = Vector2.zero;
    System.Type nullStep = typeof(ScenarioStep);
    int whatToConnect = -1;

    public void OnGUI()
    {
        if (scenarioController.steps == null)
            return;
        if (whatToConnect != -1)
            DrawConnectionMouseBezier();

        for (int i = 0; i < scenarioController.steps.Count; i++)
            if (scenarioController.steps[i].GetType() != nullStep && scenarioController.steps[i] != null)
            DrawNode(i, scenarioController.steps[i], scenarioController.offset);
        Event currEvent = Event.current;
        CheckMouseDrag(currEvent);
        Repaint();
    }

   
    private void DrawNode(int id, ScenarioStep scenarioStep, Vector2 offset)
    {
        Rect offsetedRect = new Rect(scenarioController.rects[id].position, scenarioController.rects[id].size);
        offsetedRect.position += offset;
        DrawConnections(id, scenarioStep, offsetedRect);
        scenarioStep.DrawEditorGUI(offsetedRect, scenarioController);        
        Rect addRect = new Rect(offsetedRect.x + offsetedRect.width-5f, offsetedRect.y -10f + offsetedRect.height / 2f, 20f, 20f);
        if (GUI.Button(addRect, "+"))
        {
            whatToConnect = id;
        }
    }

    private void DrawConnections(int id, ScenarioStep scenarioStep, Rect offsetedRect)
    {
        Handles.BeginGUI();
        Handles.color = Color.black;
        if (scenarioStep.next != null) {
            foreach(var i in scenarioStep.next) {
                Rect r = scenarioController.rects[i];
                DrawBezier(offsetedRect.position + new Vector2(offsetedRect.width, offsetedRect.height / 2f),
                                    r.position + new Vector2(0f, r.height/2f) + scenarioController.offset);
            }
        }
        Handles.EndGUI();
    }

    public void DrawBezier(Vector3 start, Vector3 end)
    {
        Vector3 startPos = new Vector3(start.x, start.y, 0);
        Vector3 endPos = new Vector3(end.x, end.y, 0);
        Vector3 startTan = startPos + Vector3.right * 50;
        Vector3 endTan = endPos + Vector3.left * 50;
        /*Color shadowCol = new Color(0, 0, 0, 0.06f);
       for (int i = 0; i < 3; i++) // Draw a shadow
           Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);*/
        Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 3);
    }

    int clickId = -1;
    private void CheckMouseDrag(Event e)
    {
        switch (e.type)
        {
            case EventType.MouseDown:
                Debug.Log(e.button);
                lastMouseClick = e.mousePosition;
                lastMousePos = e.mousePosition;
                clickId = GetClickedNode();
                if (whatToConnect != -1 && clickId!=whatToConnect && clickId!=-1)
                {
                    if (e.button == 1)
                        whatToConnect = -1;
                    else {
                        ScenarioStep temp = scenarioController.steps[whatToConnect];
                        if (temp.next != null && temp.next.Contains(clickId))
                            temp.RemoveNextId(clickId);
                        else {
                            temp.AddNext(clickId, scenarioController);
                            scenarioController.steps[clickId].AddPrev(whatToConnect);
                        }
                        whatToConnect = -1;
                    }
                }
                break;
           
            case EventType.MouseDrag:
                if (e.mousePosition.y < 0f || lastMousePos.y<0f)
                    break;
                if (clickId == -1)
                    scenarioController.offset += e.mousePosition - lastMousePos;
                else {
                    Rect r = scenarioController.rects[clickId];
                    r.position += e.mousePosition - lastMousePos;
                    scenarioController.rects[clickId] = r;
                }
                lastMousePos = e.mousePosition;
                break;

            case EventType.MouseUp:
                clickId = -1;
                break;

            case EventType.ContextClick:
                if (whatToConnect !=-1)
                {
                    whatToConnect = -1;
                    break;
                }
                
                lastMouseClick = lastMouseClick = e.mousePosition;
                clickId = GetClickedNode();
                if (clickId == -1)
                    ContextClick(e);
                else
                    NodeContextClick(e);
                break;
            case EventType.Repaint:
                lastMousePos = e.mousePosition;
                break;
        }
       
    }

    public void DrawConnectionMouseBezier()
    {
        Rect offsetedRect = new Rect(scenarioController.rects[whatToConnect].position, scenarioController.rects[whatToConnect].size);
        offsetedRect.position += scenarioController.offset;
        offsetedRect.x = offsetedRect.x + offsetedRect.width - 5f;
        offsetedRect.y = offsetedRect.y - 10f + offsetedRect.height / 2f;
        DrawBezier(offsetedRect.position, lastMousePos);
    }

    private void NodeContextClick(Event e)
    {
        lastMousePos = e.mousePosition;
        GenericMenu menu = new GenericMenu();
        menu.AddItem(new GUIContent("Remove step"), false, RemoveStep);
        menu.ShowAsContext();
        e.Use();
    }

    private int GetClickedNode()
    {
        Rect r;
        for (int i = 0; i < scenarioController.steps.Count; i++)
        {
            if (scenarioController.steps[i].GetType() == nullStep)
                continue;
            r = scenarioController.rects[i];
            if (r.Contains(lastMousePos - scenarioController.offset))
                return i;
        }
        return -1;
    }

    private void ContextClick(Event e)
    {
        lastMousePos = e.mousePosition;
        GenericMenu menu = new GenericMenu();
        foreach (var c in stepTypes)
        {
            menu.AddItem(new GUIContent("Add step/" + c.Name), false, AddStep, c);
        }
        menu.ShowAsContext();
        e.Use();
    }

    private void RemoveStep()
    {
        ScenarioStep temp = scenarioController.steps[clickId];
        var prev = temp.Prev;
        for (int i = 0; i < prev.Count; i++)
        {
            scenarioController.steps[prev[i]].RemoveNextId(clickId);
        }

        if (clickId == scenarioController.steps.Count - 1)
            scenarioController.steps.RemoveAt(scenarioController.steps.Count - 1);
        else 
            scenarioController.steps[clickId] = new ScenarioStep();
    }

    public void AddStep(object actionName)
    {
        int freeId = -1;
        for (int i = 0; i < scenarioController.steps.Count; i++)
            if (scenarioController.steps[i].GetType() == nullStep || scenarioController.steps[i] == null)
            {
                freeId = i;
                break;
            }

        System.Type type = (System.Type)actionName;
        ScenarioStep obj = (ScenarioStep)Activator.CreateInstance(type);
        obj.id = scenarioController.steps.Count;
        if (freeId == -1 && scenarioController.steps.Count < scenarioController.rects.Count)
        {
            freeId = obj.id;
            scenarioController.steps.Add(obj);
        }
        if (freeId == -1)
        {
            scenarioController.steps.Add(obj);
            obj.id = scenarioController.steps.Count - 1;
            Rect step = new Rect(lastMousePos.x - scenarioController.offset.x, lastMousePos.y - scenarioController.offset.y, 150f, 100f);
            step.position = lastMousePos - scenarioController.offset;
            scenarioController.rects.Add(step);
        }
        else
        {
            scenarioController.steps[freeId] = obj;
            obj.id = freeId;
            Rect step = new Rect(lastMousePos.x - scenarioController.offset.x, lastMousePos.y - scenarioController.offset.y, 150f, 100f);
            step.position = lastMousePos - scenarioController.offset;
            scenarioController.rects[freeId] = step;
        }
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }

    public void OnLostFocus()
    {
        scenarioController.Save();
    }
}

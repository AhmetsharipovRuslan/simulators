﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastCollisionEnter : MonoBehaviour {
    public GameObject lastObj;
    public void OnCollisionEnter(Collision coll)
    {
        lastObj = coll.gameObject;
    }
}

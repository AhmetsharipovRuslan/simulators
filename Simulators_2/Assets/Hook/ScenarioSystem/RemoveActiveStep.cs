﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveActiveStep : ScenarioStep
{
    public GameObject go;
    public int stepId;
    public override bool DoAction(Transform interactionArea = null)
    {
        if (!isDone)
        {
            Debug.Log("ObjName == "+go.name);
            go.GetComponent<ScenarioController>().RemoveActiveStep(stepId);
            isDone = true;
        }
        return isDone;
    }
}

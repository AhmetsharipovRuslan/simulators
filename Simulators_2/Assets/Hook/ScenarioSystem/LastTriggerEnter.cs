﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastTriggerEnter : MonoBehaviour{
    public GameObject lastObj;
    public void OnTriggerEnter(Collider coll)
    {
        lastObj = coll.gameObject;
    }
}


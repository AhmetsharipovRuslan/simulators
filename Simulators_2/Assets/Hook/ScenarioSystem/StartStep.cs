﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class StartStep : ScenarioStep {

    public override bool DoAction(Transform interactionArea)
    {
        this.isDone = true;
        return true;
    }
#if UNITY_EDITOR
    public override void DrawEditorGUI(Rect offsetedRect, ScenarioController scenarioController)
    {
        GUILayout.BeginArea(offsetedRect);
        Rect r = new Rect(0f, 0f, offsetedRect.width, offsetedRect.height);
        if (this.isDone)
            GUI.backgroundColor = Color.green;
        GUI.Box(r, "");
        GUILayoutOption width = GUILayout.Width(offsetedRect.width - 5);
        EditorGUI.indentLevel = 1;
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(this.GetType().Name + " " + this.id);
        Rect rr = scenarioController.rects[id];
        rr.height = 60;
        scenarioController.rects[id] = rr;
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
#endif
}

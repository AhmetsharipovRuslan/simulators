﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitForButtonClick : ScenarioStep {
    public GameObject go;
    bool actionAdded = false;
    public override bool DoAction(Transform interactionArea = null)
    {
        if (!actionAdded)
        {
            actionAdded = true;
            go.GetComponent<Button>().onClick.AddListener(()=> {
                isDone = true;
            });
        }
        return isDone;
    }
}

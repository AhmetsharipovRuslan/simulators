﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class LocalMove : ScenarioStep
{
    public GameObject obj;
    public float speed;
    public Vector3 targetPos;
    public bool lerp = false;
    public override bool DoAction(Transform interactionArea = null)
    {
        if (lerp)
            obj.transform.localPosition = Vector3.Lerp(obj.transform.localPosition, targetPos, speed * Time.deltaTime);
        else 
            obj.transform.localPosition = Vector3.MoveTowards(obj.transform.localPosition, targetPos, speed * Time.deltaTime);
        if ((obj.transform.localPosition - targetPos).sqrMagnitude < 0.001f)
            return isDone = true;
        return false;
    }
}


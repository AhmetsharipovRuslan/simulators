﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckCollision : ScenarioStep {
    public GameObject first;
    public GameObject second;
    public override bool DoAction(Transform interactionArea = null)
    {
        return first.GetComponent<LastCollisionEnter>().lastObj == second;
    }
}

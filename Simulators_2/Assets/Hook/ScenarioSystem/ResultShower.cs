﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultShower : MonoBehaviour {
    public GameObject[] panels;
    int result;
    public void SetResult(int result) {
        this.result = result;
    }

    public void ShowResult()
    {
        panels[result].SetActive(true);
    }
}

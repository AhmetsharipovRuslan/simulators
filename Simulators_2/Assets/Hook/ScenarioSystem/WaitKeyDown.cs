﻿using UnityEngine;

public class WaitKeyDown : ScenarioStep
{
    public string key;
    public override bool DoAction(Transform interactionArea = null)
    {
        return Input.GetKeyDown(key);
    }
}

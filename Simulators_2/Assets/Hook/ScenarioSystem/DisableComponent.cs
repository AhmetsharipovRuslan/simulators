﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableComponent : ScenarioStep {
    public GameObject go;
    public string component;
    public bool active;
    public override bool DoAction(Transform interactionArea = null)
    {
        Debug.Log(component+ " " + go.name);
        ((MonoBehaviour)go.GetComponent(component)).enabled = active;
        isDone = true;
        return true;
    }
}

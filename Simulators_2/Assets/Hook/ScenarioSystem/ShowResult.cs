﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowResult : ScenarioStep
{
    public GameObject go;

    public override bool DoAction(Transform interactionArea = null)
    {
        go.GetComponent<ResultShower>().ShowResult();
        isDone = true;
        return true;
    }
}

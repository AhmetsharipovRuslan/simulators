﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetText : ScenarioStep
{
    public GameObject go;
    public string text;

    public override bool DoAction(Transform interactionArea = null)
    {
        go.GetComponent<Text>().text = text;
        isDone = true;
        return true;
    }
}

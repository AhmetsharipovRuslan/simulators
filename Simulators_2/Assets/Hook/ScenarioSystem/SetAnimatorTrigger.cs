﻿using UnityEngine;

public class SetAnimatorTrigger : ScenarioStep
{
    public GameObject obj;
    public string trigger;
    public override bool DoAction(Transform interactionArea = null)
    {
        obj.GetComponent<Animator>().SetTrigger(trigger);
        isDone = true;
        return true;
    }
}

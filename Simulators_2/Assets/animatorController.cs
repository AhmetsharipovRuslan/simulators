﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class animatorController : MonoBehaviour
{
    ObjectsOutline outline;
    public int intNum = 0;
    public int intVal = 0;
    public Animator animator;
    public Image img;
    public Color col;
    public UnityEvent onObjectClick;
    public UnityEvent onMouseEnter;
    public UnityEvent onMouseExit;
    public void Awake()
    {
        outline = GetComponent<ObjectsOutline>();
        if (outline != null)
            outline.enabled = false;
    }
    public void setAnimatorValue()

    {
        switch (intNum)
        {
            case 0:
                animator.SetInteger("selectedBelt", intVal);
                break;
            case 1:
                animator.SetInteger("selectedWire", intVal);
                break;
            case 2:
                animator.SetInteger("selectedSide", intVal);
                break;
        }
        img.color = col;
    }
    public void MouseEnter()
    {
        if (onMouseEnter != null)
            onMouseEnter.Invoke();
    }

    public void MouseExit()
    {
        if (onMouseExit != null)
            onMouseExit.Invoke();
    }

    public void MouseDown()
    {
        setAnimatorValue();
        if (onObjectClick != null)
            onObjectClick.Invoke();
        Debug.Log("DOWN" + name);
    }

}

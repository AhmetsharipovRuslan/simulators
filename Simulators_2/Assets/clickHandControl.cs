﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;
using HTC.UnityPlugin.Pointer3D;
using UnityEngine.EventSystems;

public class clickHandControl : MonoBehaviour, IPointer3DPressEnterHandler
{
    public HandRole role;
    public PhysicsRaycastMethod physics_raycast;
    private GameObject hittedGO;
    private RaycastHit hit;
    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        List<RaycastResult> results = new List<RaycastResult>();
        physics_raycast.Raycast(ray, 5f, results);

        if (results.Count > 0)
        {
            Debug.Log(results[0].gameObject.name);
            if (results[0].gameObject != hittedGO)
            {
                if (hittedGO != null)
                    hittedGO.GetComponent<animatorController>().MouseExit();
                if (results[0].gameObject.GetComponent<animatorController>() != null)
                {
                    hittedGO = results[0].gameObject;
                    hittedGO.GetComponent<animatorController>().MouseEnter();
                }
                else
                    hittedGO = null;
            }
        }
        if (ViveInput.GetPressDownEx(role, ControllerButton.Trigger))
        {
            TriggerPressed();
        }

    }
    void TriggerPressed()
    {
        if (hittedGO != null)
        {
            hittedGO.GetComponent<animatorController>().MouseDown();
        }
    }

    public void OnPointer3DPressEnter(Pointer3DEventData eventData)
    {
        Debug.Log("enter 3D");
    }
}
